
civicrm_countblock is a simple module that creates blocks for displaying
civicrm group count information.  An HTML format string can be defined
with substitutable parameters for the group count (%count) and group name
(%group).  The group count can be rounded to the tens, hundreds, or thousands
position.

Installation
------------

Copy civicrm_countblock.module to your module directory, enable it on the
admin modules page, edit the block settings on the admin/block page, and
create additional blocks on the admin/settings/civicrm_countblock page.

Author
------
Doug Green
douggreen@douggreenconsulting.com
